# SmappeeKit #

An implementation of the [Smappee API](https://smappee.atlassian.net/wiki/display/DEVAPI/SmappeeDevAPI+Home) in Swift.
This project is dependent on the [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) library

### Note ###
The current version of the source code is made for the Xcode 6.3 beta version of Swift.

### TODO ###
* Finish this README text
* Add full example project
* Add a PodSpec so SmappeeKit can be used with CocoaPods

